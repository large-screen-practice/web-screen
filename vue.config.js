const path = require('path');
function resolve(dir) {
    return path.join(__dirname, dir)
}
const BaseUrl = require("./src/config.js");
module.exports = {
    lintOnSave: true,
    assetsDir: "static",
    publicPath : './',
    chainWebpack: (config) => {
        config.resolve.alias
            .set('@$', resolve('src'))
            .set('assets', resolve('src/assets'))
            .set('components', resolve('src/components'))
            .set('layout', resolve('src/layout'))
            .set('base', resolve('src/base'))
            .set('static', resolve('src/static'))
    },
    devServer: {
        port: 3000,
        open: true, //配置自动启动浏览器
        // proxy: 'http://localhost:4000' // 配置跨域处理,只有一个代理
        proxy: {
            // [BaseUrl.ROOT]: {
            //     // target: 'http://localhost:8080/',   // 需要请求的地址
            //     changeOrigin: true,  // 是否跨域
            //     pathRewrite: {
            //         [`^${BaseUrl.ROOT}`]: '/'  // 替换target中的请求地址
            //     }
            // }
            '/stview': {
                // target: 'http://localhost:8080/',   // 需要请求的地址
                target: 'http://140.249.48.42:8089/stview',
                changeOrigin: true,  // 是否跨域
                pathRewrite: {
                    '^/stview': '/stview'  // 替换target中的请求地址
                }
            }
        }
    }
}