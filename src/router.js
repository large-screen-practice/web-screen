/**
 * 开发依赖组件
 */
import Vue from 'vue'
import Router from 'vue-router'
import home from './views/home/home'
import Rural from './views/home/Rural-whole'


Vue.use(Router)


let routeList = [
  {
    path: '/',
    name: '',
    redirect: '/Rural'
  }, {
    path: '/home',
    name: '',
    component: home
  },
  {
    path: '/Rural',
    name: '',
    component: Rural
  }
]

// fetch('/stview/tightening/getMachineList').then((res) => {
//   let data = res.data.Rows
//   for(let i in data){
//     routeList.push({ path: '/'+data[i].picture, component:njj  })
//   }
// })
// console.log(routeList)
let route = new Router({
  mode: 'hash',
  routes: routeList
})
export default route