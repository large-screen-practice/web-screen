import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// import echarts from 'echarts'
import * as echarts from 'echarts';
import 'echarts-gl'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import initChart from './assets/echartInit'
import { post, fetch } from "./assets/httpRequest";
import geoJson from './utils/china.json'

// 将自动注册所有组件为全局组件
import dataV from '@jiaminghi/data-view'
// 引入echarts
Vue.use(dataV)
Vue.use(ElementUI);
echarts.registerMap('china', geoJson);
Vue.config.productionTip = false
Vue.prototype.$get = fetch;
Vue.prototype.$post = post;
Vue.prototype.$chart = echarts
// Vue.prototype.$chart = echarts;
Vue.prototype.$initChart = initChart.initChart
Vue.directive('tap', {
    bind: function (el, binding) {
        var startTx, startTy, endTx, endTy, startTime, endTime;

        el.addEventListener("touchstart", function (e) {
            var touch = e.touches[0];
            startTx = touch.clientX;
            startTy = touch.clientY;
            startTime = +new Date()
        }, false);

        el.addEventListener("touchend", function (e) {
            endTime = +new Date()
            if (endTime - startTime > 300) {
                // 若点击事件过长，不执行回调
                return
            }
            var touch = e.changedTouches[0];
            endTx = touch.clientX;
            endTy = touch.clientY;
            if (Math.abs(startTx - endTx) < 6 && Math.abs(startTy - endTy) < 6) {
                // 若点击期间手机移动距离过长，不执行回调
                var method = binding.value.method;
                var params = binding.value.params;
                method(params);
            }
        }, false);
    }
})



new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
