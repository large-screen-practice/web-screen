// import {
//     baseUrl, //引入baseUrl 
//   } from "../config/env";
  import axios from 'axios';
  //axios.defaults.timeout = 30000; //设置请求时间
  // axios.defaults.baseURL = 'http://140.249.48.42:8089';//设置默认接口地址
  axios.defaults.baseURL = window.baseUrl
  // axios.defaults.baseURL = "http://192.168.1.171:8080/centerScada"
  /**
   * 封装get方法
   * @param url
   * @param data
   * @returns {Promise}
  */
  export function fetch(url,params={}){
      return new Promise((resolve,reject) => {
        Object.assign(params,{
          timestamp:new Date().getTime()
        })
        axios.get(url,{
          params:params
        })
        .then(response => {
          resolve(response.data);
        })
        .catch(err => {
          reject(err)
        })
      })
    }
  /**
   * 封装post请求
   * @param url
   * @param data
   * @returns {Promise}
   */
   
   export function post(url,data = {}){
     return new Promise((resolve,reject) => {
       axios.post(url,data)
            .then(response => {
              resolve(response.data);
            }).catch(err => {
              reject(err)
            })
     })
   }