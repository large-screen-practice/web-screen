import echarts from 'echarts'

export default {
    /* 绘制echarts图表
    * @method initChart
    * @parmas chartOptions <Object> {
    *   id: echarts容器id （bar or line）
    *   chartType: echarts图表类型 （bar or line）
    *   seriesData: 图表数据 （bar or line）
    *   xAxisBoundaryGap： 坐标轴两边留白策略 （bar）
    *   xAxisData: 坐标轴数据（bar or line）
    *   isSmooth: 平滑线（line）
    *   lineColor: 线颜色（line）
    *   areaStyle: 阴影颜色（line）
    *   barBorderRadius: 柱形圆角（bar）
    *   subData: 第二组数据{
    *     seriesData: 第二组的数据
    *     subLineColor: 第二组数据线的颜色
    *   }
    * }
    * 
    */
    initChart(chartOptions) {
      // console.log(chartOptions.lineColor);
        let anyChart = echarts.init(
            document.getElementById(chartOptions.id)
          );
          // 绘制图表
          anyChart.setOption({
            title: {
              show: false //标题配置
            },
            tooltip: {
              trigger: 'axis'
            },
            grid: {
              top: "15%",
              right: "5%",
              bottom: "20%",
              left: chartOptions.gridLeft?chartOptions.gridLeft:"8%"
            },
            xAxis: {
              show: true,
              boundaryGap: true,
              axisLine: {
                show: true,
                lineStyle: {
                  width: 2,
                  color: "#2D4A6B"
                }
              },
              axisLabel: (function(){
                if(chartOptions.axisLabel){
                  return {  
                    color: "#4F9EFA",
                    interval:0,  
                    rotate:40  
                 } 
                }else{
                  return {
                    color: "#4F9EFA"
                  }
                }
              })(),
              spliteLine: {
                show: false,
                lineStyle: {
                  color: "#C23531"
                }
              },
              data: chartOptions.xAxisData
            },
            yAxis: (function(){
              let one = {
                name: chartOptions.yAxisName,
                axisLine: {
                  show: true,
                  lineStyle: {}
                },
                nameTextStyle:{
                  fontSize:'14',
                  color: '#1298DF'
                },
                splitLine: {
                  show: true,
                  lineStyle: {
                    width: 1,
                    color: "#3A72B3",
                    style: "dotted",
                    opacity: 0.3
                  }
                },
                // axisLine: {
                //   show: true,
                //   lineStyle: {
                //     width: 2,
                //     color: "#2D4A6B"
                //   }
                // },
                axisLabel: {
                  color: "#4F9EFA",
                  formatter : '{value}' //chartOptions.isRate?'{value} %':
                },
                spliteLine: {
                  show: false,
                  lineStyle: {
                    color: "#C23531"
                  }
                }
              }
              if(!chartOptions.isTwoYAixs){
                return one
              }else{
                return [
                  one,{
                    name: chartOptions.yAxisName2,
                    type : 'value',
                    axisLine: {
                      show: true,
                      lineStyle: {}
                    },
                    nameTextStyle:{
                      fontSize:'14',
                      color: '#1298DF'
                    },
                    splitLine: {
                      show: true,
                      lineStyle: {
                        width: 1,
                        color: "#3A72B3",
                        style: "dotted",
                        opacity: 0.3
                      }
                    },
                    // axisLine: {
                    //   show: true,
                    //   lineStyle: {
                    //     width: 2,
                    //     color: "#2D4A6B"
                    //   }
                    // },
                    axisLabel: {
                      color: "#4F9EFA"
                    },
                    spliteLine: {
                      show: false,
                      lineStyle: {
                        color: "#C23531"
                      }
                    }
                  }
                ]
              }
            })(),
            legend:(function(){
              if(chartOptions.legend){
                let legend = chartOptions.legend
                return legend
              }
            })(),
            series: (function() {
              let currentSeries = [
                {
                  data: chartOptions.seriesData,
                  type: chartOptions.chartType,
                  symbol: "circle",
                  hoverAnimation: true,
                  name: chartOptions.seriesName?chartOptions.seriesName:'指标值',
                  barWidth: 20,
                  smooth: chartOptions.isSmooth, //平滑线
                  lineStyle: {
                    color: chartOptions.lineColor
                    // color:'red'
                  },
                  markLine: {
                    silent: true,
                    symbol: "none", //去掉警戒线最后面的箭头
                    data: [
                      {
                        silent: true, //鼠标悬停事件
                        lineStyle: {
                          type: "solid", //线类型
                          color: "#F97C5E", //颜色
                          width: 1
                        },
                        yAxis: chartOptions.yjValue?chartOptions.yjValue:0 // 警戒线的标注值，可以有多个yAxis,多条警示线   或者采用   {type : 'average', name: '平均值'}，type值有  max  min  average，分为最大，最小，平均值
                      },
                      {
                        silent: true, //鼠标悬停事件
                        lineStyle: {
                          type: "solid", //线类型
                          color: "#185195", //颜色
                          width: 1
                        },
                        yAxis: chartOptions.jcValue?chartOptions.jcValue:0 // 警戒线的标注值，可以有多个yAxis,多条警示线   或者采用   {type : 'average', name: '平均值'}，type值有  max  min  average，分为最大，最小，平均值
                      }
                    ]
                  },
                  areaStyle: chartOptions.areaStyle,
                  itemStyle: {
                    normal: {
                      color: chartOptions.barItemColor?chartOptions.barItemColor:"#1298DF",
                      // color:function(params) {
                      //       // build a color map as your need.
                      //       var colorList = [
                      //         '#0066cc','#33ccff'
                      //       ];
                      //       return colorList[params.dataIndex]
                      //   },
                      //柱形图圆角，初始化效果
                      barBorderRadius: chartOptions.barBorderRadius
                    }
                  }
                }
                // /**
                //  *
                //  */
                // {
                //   data: [],
                //   type: chartOptions.chartType,
                //   isSmooth: "",
                //   lineStyle: {
                //     color: ""
                //   }
                // }
              ];
              
              if (chartOptions.subData) {
                for(let i = 0;i < chartOptions.subData.length; i++ ){
                  
                  currentSeries[i+1]={}
                  currentSeries[i+1]['name'] = chartOptions.subData[i].name?chartOptions.subData[i].name:''
                  currentSeries[i+1]['type'] = chartOptions.subData[i].type?chartOptions.subData[i].type:'bar'
                  currentSeries[i+1]['data'] = chartOptions.subData[i].seriesData?chartOptions.subData[i].seriesData:[] //第二组数据
                  if(!chartOptions.subData[i]['isSmooth']){
                    currentSeries[i+1]['smooth'] = chartOptions.isSmooth?chartOptions.isSmooth:true
                  }else{
                    currentSeries[i+1]['smooth'] = false
                  }
                  
                  currentSeries[i+1]['symbol'] = "none";
                  currentSeries[i+1]['barWidth'] = 20
                  if(chartOptions.isTwoYAixs){
                    currentSeries[i+1]['yAxisIndex'] = '1'
                  }
                  currentSeries[i+1]['itemStyle'] = {}
                  currentSeries[i+1]['itemStyle']['color'] = chartOptions.subData[i].itemColor? chartOptions.subData[i].itemColor:'#33ccff'
                  // currentSeries[i+1]['color'] = ['#33ccff','']
                }
              }
              //console.log(currentSeries);
              return currentSeries;
            })()
          });
    }
}