import Vue from 'vue'
import Axios from 'axios'
import initChart from './echartInit'
export default {
    sendAjax(apiPath ,Options ,params ,zbName) {
        let Option = Options
        Axios.get(apiPath,{params: params}).then((res)=>{
            let data = res.data.data.Rows
            let xAxisData = []
            let seriesData = []
            let yjValue = 0
            let jcValue = 0
            let units = ''
            for(let i in data){
              if(i == "tTile"){
                xAxisData = data[i].slice(1)
              }else{
                jcValue = data[i].dataForAlarm['基础值']
                yjValue = data[i].dataForAlarm['预警值']
                seriesData = data[i].dataForAlarm['指标值'].slice(1)
                units = data[i].dataForAlarm['指标值'][0]
                //seriesData = data[i].option[zbName].slice(1)
              }
            }
            Options['seriesName'] = zbName
            Option['jcValue'] = jcValue[0]
            Option['yjValue'] = yjValue[0]
            Option['xAxisData'] = xAxisData
            Option['seriesData'] = seriesData
            Option['yAxisName'] = units
            initChart.initChart(Option)
          })
        },
    sendAjaxPPM(apiPath ,Options ,params) {
      this.$axios.get(apiPath,{params:params}).then((res) => {
        let data = res.data.data.Rows
        let data2 = {}
        let title = []
        let subData = [];
        let yjValue = 0
        let jcValue = 0
        for(let n in data){
          if(n!='tTile'){
            data2 = data[n]
          }else{
            title = data[n].slice(1)
          }
        }
        if(data2.series){
          for(let i =0;i < data2.series.length/title.length;i++){
            let obj = {
              name: data2.series[i],
              seriesData: [],
              type: 'bar'
            }
            for(let j in data2.series){
              let a = (j-1)%(data2.series.length/title.length) == i
              if(a){
                obj.seriesData.push(data2.dataForAlarm['指标值'][j])
                jcValue = data2.dataForAlarm['基础值'][j]
                yjValue = data2.dataForAlarm['预警值'][j]
              }
            }
            subData.push(obj)
          }
          if(subData.length>=2){
            subData[1]['itemColor'] = '#7c4dff'
          }else if(subData.length>=3){
            subData[1]['itemColor'] = '#7c4dff'
            subData[2]['itemColor'] = '#64ffda'
          }
          Options.yjValue = yjValue
          Options.jcValue = jcValue
          Options.xAxisData = title
          Options.seriesName = subData[0].name
          Options.seriesData = subData[0].seriesData
          Options.subData = subData.slice(1)
          Options['yAxisName'] = 'PPM'
        }
        
        initChart.initChart(Options)
      })
    },
    sendAjaxPPM2(apiPath ,Options ,params,zbName) {
      this.$axios.get(apiPath,{params:params}).then((res) => {
        let data = res.data.data
        let data2 = {}
        let xilie = []
        let title = []
        let subData = [];
        if(data.table.length>0){
          for(let n in data.Rows){
            if(n!='tTile'){
              data2 = data.Rows[n]
            }else{
              title = data.Rows[n].slice(1)
            }
          }
          for(let x in data.map){
            let obj = {name:x,date:[]}
            for(let d in title){
              obj.date.push({date:title[d],num:0})
            }
            xilie.push(obj)
          }
          for(let item in xilie){
            for(let da in xilie[item].date){
              for(let t in data.table){
                if(params.dataCycle == 'm'){
                  if(xilie[item].date[da].date == data.table[t].month&&xilie[item].name == data.table[t].series){
                    xilie[item].date[da].num = data.table[t][zbName]
                  }
                }else{
                  Options['axisLabel'] = true
                  if(xilie[item].date[da].date == data.table[t].date&&xilie[item].name == data.table[t].series){
                    xilie[item].date[da].num = data.table[t][zbName]
                  }
                }
              }
            }
          }
          for(let xx in xilie){
            let obj = {
              name: xilie[xx].name,
              seriesData: [],
              type: 'bar'
            }
            for(let dd in xilie[xx].date){
              obj.seriesData.push(xilie[xx].date[dd].num)
            }
            subData.push(obj)
          }
          if(subData.length>=2){
            subData[1]['itemColor'] = '#7c4dff'
          }else if(subData.length>=3){
            subData[1]['itemColor'] = '#7c4dff'
            subData[2]['itemColor'] = '#64ffda'
          }
          Options.xAxisData = title
          Options.seriesName = subData[0].name
          Options.seriesData = subData[0].seriesData
          Options.subData = subData.slice(1)
        }
        console.log(subData)
        Options['yAxisName'] = 'PPM'
        initChart.initChart(Options)
      })
    }
}